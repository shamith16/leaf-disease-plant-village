python resnet18_run.py --arch 'resnet18' --dataseparate '10-90' --has_weights 'True' --dataroot '/home/h/Downloads/plantvillage_deeplearning_paper_dataset' --saveroot '/media/h/resnet101' && \
    python resnet18_run.py --arch 'resnet18' --dataseparate '20-80' --has_weights 'True' --dataroot '/home/h/Downloads/plantvillage_deeplearning_paper_dataset' --saveroot '/media/h/resnet101' && \
    python resnet18_run.py --arch 'resnet18' --dataseparate '40-60' --has_weights 'True' --dataroot '/home/h/Downloads/plantvillage_deeplearning_paper_dataset' --saveroot '/media/h/resnet101' && \
    python resnet18_run.py --arch 'resnet18' --dataseparate '50-50' --has_weights 'True' --dataroot '/home/h/Downloads/plantvillage_deeplearning_paper_dataset' --saveroot '/media/h/resnet101' && \
    python resnet18_run.py --arch 'resnet18' --dataseparate '60-40' --has_weights 'True' --dataroot '/home/h/Downloads/plantvillage_deeplearning_paper_dataset' --saveroot '/media/h/resnet101' && \
    python resnet18_run.py --arch 'resnet18' --dataseparate '80-20' --has_weights 'True' --dataroot '/home/h/Downloads/plantvillage_deeplearning_paper_dataset' --saveroot '/media/h/resnet101'
